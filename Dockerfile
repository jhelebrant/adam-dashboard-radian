FROM registry.nic.cz/adam/adam-dashboard:devel

RUN apt-get -yqq update
RUN apt-get -yqq install python3-pip
RUN pip3 install radian
WORKDIR /adam-dashboard
CMD radian